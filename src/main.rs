use async_trait::async_trait;
use bytes::Bytes;
use clap::{value_parser, Parser};
use failure::{bail, Error};
use fuse3::{path::prelude::*, MountOptions};
use futures::stream::BoxStream;
use futures_util::stream::{self, Empty, StreamExt};
use indexmap::IndexMap;
use itertools::Itertools;
use log::{debug, error, warn};
use std::{
    cmp::max,
    ffi::OsStr,
    path::PathBuf,
    str,
    sync::Arc,
    time::{Duration, SystemTime},
};
use tokio_retry::{
    strategy::{jitter, ExponentialBackoff},
    Retry,
};
use users::{get_current_gid, get_current_uid};

mod e621;
use e621::E621;

// TODO analyse performance, readdir is really slow

const GIT_URL: &str = "https://gitlab.com/TilCreator/e621fs";
const DEFAULT_TIME: SystemTime = SystemTime::UNIX_EPOCH;

const BACKOF_DELAY: u64 = 100; // ms
const BACKOF_FACTOR: u64 = 3;
const BACKOF_MAX_DELAY: u64 = 10_000; // ms

#[derive(Debug, PartialEq, Eq, Clone)]
struct CustomPath {
    tags: Vec<String>,
    has_delimiter: bool,
    id: Option<usize>,
}

impl CustomPath {
    fn tags_to_string(&self) -> String {
        self.tags
            .iter()
            .intersperse(&String::from(" "))
            .cloned()
            .collect::<String>()
    }
}

impl TryFrom<PathBuf> for CustomPath {
    type Error = Error;

    fn try_from(path: PathBuf) -> Result<Self, Error> {
        let mut tags = vec![];
        let mut has_delimiter = false;
        let mut id = None;

        for component in path.components() {
            if let std::path::Component::Normal(name) = component {
                if name == "@" {
                    if has_delimiter {
                        bail!("More than one delimiter \"@\" in path found");
                    } else {
                        has_delimiter = true;
                    }
                } else if has_delimiter {
                    if id.is_some() {
                        bail!("Only one filename after delimiter allowed");
                    } else {
                        id = Some({
                            let component = component.as_os_str().to_string_lossy();
                            component
                                .split('.')
                                .next()
                                .unwrap_or(&component)
                                .parse::<usize>()?
                        });
                    }
                } else {
                    tags.append(
                        &mut name
                            .to_string_lossy()
                            .split(' ')
                            .map(String::from)
                            .collect(),
                    );
                }
            }
        }

        Ok(Self {
            tags,
            has_delimiter,
            id,
        })
    }
}

#[derive(Clone)]
struct E621Fs {
    e621: Arc<E621>,
    metadata_cache_time: Duration,
    max_retries: usize,
    check_file_path: bool,
    preload_neighbors: bool,
    timeout: f64,
    max_page_count: usize,
}

impl E621Fs {
    fn new(
        api_key: Option<String>,
        nick: Option<String>,
        max_retries: usize,
        metadata_cache_size: usize,
        metadata_cache_time: Duration,
        file_cache_size: usize,
        file_cache_time: Duration,
        check_file_path: bool,
        preload_neighbors: bool,
        timeout: f64,
        max_page_count: usize,
    ) -> Self {
        assert!(
            if api_key.is_some() {
                nick.is_some()
            } else {
                true
            },
            "Nick has to be set for api key to be used"
        );

        Self {
            e621: Arc::new(E621::new(
                api_key,
                nick,
                metadata_cache_size,
                metadata_cache_time,
                file_cache_size,
                file_cache_time,
            )),
            metadata_cache_time,
            check_file_path,
            preload_neighbors,
            max_retries,
            timeout,
            max_page_count,
        }
    }
}

#[async_trait]
impl PathFilesystem for E621Fs {
    type DirEntryStream = Empty<fuse3::Result<DirectoryEntry>>;
    type DirEntryPlusStream = BoxStream<'static, fuse3::Result<DirectoryEntryPlus>>;

    async fn init(&self, request: Request) -> fuse3::Result<()> {
        debug!("FS init: request: {:?}", request);

        Ok(())
    }

    async fn destroy(&self, request: Request) {
        debug!("FS destroy: request: {:?}", request);
    }

    async fn lookup(
        &self,
        request: Request,
        parent: &OsStr,
        name: &OsStr,
    ) -> fuse3::Result<ReplyEntry> {
        let path = PathBuf::from(parent).join(name);
        let custom_path =
            CustomPath::try_from(path.clone()).map_err(|_| fuse3::Errno::new_not_exist())?;

        debug!(
            "FS lookup: path: {:?}, request: {:?} -> custom_path: {:?}",
            path, request, custom_path
        );

        if let Some(id) = custom_path.id {
            let tags = custom_path.tags_to_string();
            if let Some(post) = self
                .e621
                .is_post_in_search(
                    id,
                    if self.check_file_path {
                        Some(&tags)
                    } else {
                        None
                    },
                    false,
                    self.timeout,
                )
                .await
                .map_err(|err| {
                    error!("web request: {}", err);
                    fuse3::Errno::new_not_exist()
                })?
            {
                Ok(ReplyEntry {
                    attr: (&post).into(),
                    ttl: self.metadata_cache_time,
                })
            } else if self.check_file_path {
                warn!("getattr: Did not find post {}", id);

                Err(fuse3::Errno::new_not_exist())
            } else {
                Ok(ReplyEntry {
                    attr: FileAttr {
                        size: 0,
                        blocks: 0,
                        atime: DEFAULT_TIME,
                        mtime: DEFAULT_TIME,
                        ctime: DEFAULT_TIME,
                        kind: FileType::RegularFile,
                        perm: fuse3::perm_from_mode_and_kind(FileType::RegularFile, 0o444),
                        nlink: 0,
                        uid: get_current_uid(),
                        gid: get_current_gid(),
                        rdev: 0,
                        blksize: 1,
                    },
                    ttl: Duration::from_secs(0),
                })
            }
        } else {
            Ok(ReplyEntry {
                attr: FileAttr {
                    size: 0,
                    blocks: 0,
                    atime: DEFAULT_TIME,
                    mtime: DEFAULT_TIME,
                    ctime: DEFAULT_TIME,
                    kind: FileType::Directory,
                    perm: fuse3::perm_from_mode_and_kind(FileType::Directory, 0o444),
                    nlink: 0,
                    uid: get_current_uid(),
                    gid: get_current_gid(),
                    rdev: 0,
                    blksize: 0,
                },
                ttl: self.metadata_cache_time,
            })
        }
    }

    async fn getattr(
        &self,
        request: Request,
        path: Option<&OsStr>,
        file_handle: Option<u64>,
        flags: u32,
    ) -> fuse3::Result<ReplyAttr> {
        if let Some(path) = path {
            let path = PathBuf::from(path);
            let custom_path =
                CustomPath::try_from(path.clone()).map_err(|_| fuse3::Errno::new_not_exist())?;

            debug!(
                "FS getattr: path: {:?}, request: {:?}, file_handle: {:?}, flags: {} -> custom_path: {:?}",
                path, request, file_handle, flags, custom_path
            );

            if let Some(id) = custom_path.id {
                let tags = custom_path.tags_to_string();
                if let Some(post) = self
                    .e621
                    .is_post_in_search(
                        id,
                        if self.check_file_path {
                            Some(&tags)
                        } else {
                            None
                        },
                        true,
                        self.timeout,
                    )
                    .await
                    .map_err(|err| {
                        error!("web request: {}", err);
                        fuse3::Errno::new_not_exist()
                    })?
                {
                    Ok(ReplyAttr {
                        attr: (&post).into(),
                        ttl: self.metadata_cache_time,
                    })
                } else if self.check_file_path {
                    warn!("getattr: Did not find post {}", id);

                    Err(fuse3::Errno::new_not_exist())
                } else {
                    Ok(ReplyAttr {
                        attr: FileAttr {
                            size: 0,
                            blocks: 0,
                            atime: DEFAULT_TIME,
                            mtime: DEFAULT_TIME,
                            ctime: DEFAULT_TIME,
                            kind: FileType::RegularFile,
                            perm: fuse3::perm_from_mode_and_kind(FileType::RegularFile, 0o444),
                            nlink: 0,
                            uid: get_current_uid(),
                            gid: get_current_gid(),
                            rdev: 0,
                            blksize: 1,
                        },
                        ttl: Duration::from_secs(0),
                    })
                }
            } else {
                Ok(ReplyAttr {
                    attr: FileAttr {
                        size: 0,
                        blocks: 0,
                        atime: DEFAULT_TIME,
                        mtime: DEFAULT_TIME,
                        ctime: DEFAULT_TIME,
                        kind: FileType::Directory,
                        perm: fuse3::perm_from_mode_and_kind(FileType::Directory, 0o444),
                        nlink: 0,
                        uid: get_current_uid(),
                        gid: get_current_gid(),
                        rdev: 0,
                        blksize: 0,
                    },
                    ttl: self.metadata_cache_time,
                })
            }
        } else {
            warn!("getattr: No path");

            Err(fuse3::Errno::new_not_exist())
        }
    }

    // TODO Add page after, before versions
    async fn readdirplus(
        &self,
        request: Request,
        path: &OsStr,
        file_handle: u64,
        offset: u64,
        lock_owner: u64,
    ) -> fuse3::Result<ReplyDirectoryPlus<Self::DirEntryPlusStream>> {
        let path = PathBuf::from(path);
        let custom_path =
            CustomPath::try_from(path.clone()).map_err(|_| fuse3::Errno::new_not_exist())?;

        debug!(
            "FS readdirplus: path: {:?}, request: {:?}, file_handle: {}, offset: {}, lock_owner: {} -> custom_path: {:?}",
            path,
            request,
            file_handle,
            offset,
            lock_owner,
            custom_path,
        );

        if custom_path.tags.first() == Some(&String::from(".Trash"))
            || custom_path.tags.first() == Some(&String::from(".Trash-1000"))
            || custom_path.tags.first() == Some(&String::from(".git"))
            || custom_path.tags.first() == Some(&String::from(".envrc"))
        {
            Err(fuse3::Errno::new_not_exist())
        } else if custom_path.id.is_some() {
            Err(fuse3::Errno::new_is_not_dir())
        // check is max page count would be reached
        } else if self.max_page_count != 0
            && self.max_page_count < e621::POSTS_MAX_PAGE
            && !self
                .e621
                .get_posts(
                    &custom_path.tags_to_string(),
                    e621::POSTS_MAX_LIMIT,
                    self.max_page_count.to_string(),
                    self.timeout,
                )
                .await
                .map_err(|err| {
                    error!("web request: {}", err);
                    fuse3::Errno::new_not_exist()
                })?
                .posts
                .is_empty()
        {
            error!("max_page_count reached, canceling request");
            Err(fuse3::Errno::new_not_exist())
        } else {
            let tags = custom_path.tags_to_string();

            Ok(ReplyDirectoryPlus {
                entries: stream::unfold(
                    (offset.try_into().unwrap(), tags, self.clone()),
                    move |(i, tags, self)| async move {
                        match i {
                            0 => Some(Ok(DirectoryEntryPlus {
                                kind: FileType::Directory,
                                name: ".".into(),
                                offset: i + 1,
                                attr: FileAttr {
                                    size: 0,
                                    blocks: 0,
                                    atime: DEFAULT_TIME,
                                    mtime: DEFAULT_TIME,
                                    ctime: DEFAULT_TIME,
                                    kind: FileType::Directory,
                                    perm: fuse3::perm_from_mode_and_kind(
                                        FileType::Directory,
                                        0o444,
                                    ),
                                    nlink: 0,
                                    uid: get_current_uid(),
                                    gid: get_current_gid(),
                                    rdev: 0,
                                    blksize: 1,
                                },
                                attr_ttl: self.metadata_cache_time,
                                entry_ttl: self.metadata_cache_time,
                            })),
                            1 => Some(Ok(DirectoryEntryPlus {
                                kind: FileType::Directory,
                                name: "..".into(),
                                offset: i + 1,
                                attr: FileAttr {
                                    size: 0,
                                    blocks: 0,
                                    atime: DEFAULT_TIME,
                                    mtime: DEFAULT_TIME,
                                    ctime: DEFAULT_TIME,
                                    kind: FileType::Directory,
                                    perm: fuse3::perm_from_mode_and_kind(
                                        FileType::Directory,
                                        0o444,
                                    ),
                                    nlink: 0,
                                    uid: get_current_uid(),
                                    gid: get_current_gid(),
                                    rdev: 0,
                                    blksize: 1,
                                },
                                attr_ttl: self.metadata_cache_time,
                                entry_ttl: self.metadata_cache_time,
                            })),
                            2 if !custom_path.has_delimiter => Some(Ok(DirectoryEntryPlus {
                                kind: FileType::Directory,
                                name: "@".into(),
                                offset: i + 1,
                                attr: FileAttr {
                                    size: 0,
                                    blocks: 0,
                                    atime: DEFAULT_TIME,
                                    mtime: DEFAULT_TIME,
                                    ctime: DEFAULT_TIME,
                                    kind: FileType::Directory,
                                    perm: fuse3::perm_from_mode_and_kind(
                                        FileType::Directory,
                                        0o444,
                                    ),
                                    nlink: 0,
                                    uid: get_current_uid(),
                                    gid: get_current_gid(),
                                    rdev: 0,
                                    blksize: 1,
                                },
                                attr_ttl: self.metadata_cache_time,
                                entry_ttl: self.metadata_cache_time,
                            })),
                            i if !custom_path.has_delimiter => {
                                self.e621
                                    .get_tags_all(self.max_retries)
                                    .await
                                    .skip(
                                        <i64 as std::convert::TryInto<usize>>::try_into(i).unwrap()
                                            - 3,
                                    )
                                    .map(|tag| {
                                        tag.map_err(|err| {
                                            error!("web request: {}", err);
                                            fuse3::Errno::new_not_exist()
                                        })
                                        .map(|tag| {
                                            DirectoryEntryPlus {
                                                kind: FileType::Directory,
                                                name: tag.into(),
                                                offset: i + 1,
                                                attr: FileAttr {
                                                    size: 0,
                                                    blocks: 0,
                                                    atime: DEFAULT_TIME,
                                                    mtime: DEFAULT_TIME,
                                                    ctime: DEFAULT_TIME,
                                                    kind: FileType::Directory,
                                                    perm: fuse3::perm_from_mode_and_kind(
                                                        FileType::Directory,
                                                        0o444,
                                                    ),
                                                    nlink: 0,
                                                    uid: get_current_uid(),
                                                    gid: get_current_gid(),
                                                    rdev: 0,
                                                    blksize: 1,
                                                },
                                                attr_ttl: self.metadata_cache_time,
                                                entry_ttl: self.metadata_cache_time,
                                            }
                                        })
                                    })
                                    .next()
                                    .await
                            }
                            i => {
                                self.e621
                                    .get_posts_all(&tags, self.max_retries, self.timeout)
                                    .await
                                    .skip(
                                        <i64 as std::convert::TryInto<usize>>::try_into(i).unwrap()
                                            - 2,
                                    )
                                    .map(|post| {
                                        post.map_err(|err| {
                                            error!("web request: {}", err);
                                            fuse3::Errno::new_not_exist()
                                        })
                                        .map(|post| {
                                            DirectoryEntryPlus {
                                                kind: FileType::RegularFile,
                                                name: format!("{}.{}", post.id, post.file.ext)
                                                    .into(),
                                                offset: i + 1,
                                                attr: (&post).into(),
                                                attr_ttl: self.metadata_cache_time,
                                                entry_ttl: self.metadata_cache_time,
                                            }
                                        })
                                    })
                                    .boxed()
                                    .next()
                                    .await
                            }
                        }
                        .map(|value| (value, (i + 1, tags, self)))
                    },
                )
                .take(40)
                .boxed(),
            })
        }
    }

    async fn read(
        &self,
        request: Request,
        path: Option<&OsStr>,
        file_handle: u64,
        offset: u64,
        size: u32,
    ) -> fuse3::Result<ReplyData> {
        if let Some(path) = path {
            let custom_path = CustomPath::try_from(PathBuf::from(path))
                .map_err(|_| fuse3::Errno::new_not_exist())?;

            debug!(
                "FS read: path: {:?}, request: {:?}, file_handle: {:?}, offset: {}, size: {} -> custom_path: {:?}",
                path, request, file_handle, offset, size, custom_path
            );

            if let Some(id) = custom_path.id {
                let tags = custom_path.tags_to_string();
                if let Some(post) = self
                    .e621
                    .is_post_in_search(
                        id,
                        if self.check_file_path {
                            Some(&tags)
                        } else {
                            None
                        },
                        true,
                        self.timeout,
                    )
                    .await
                    .map_err(|err| {
                        error!("reqwest::Error: {:?}", err);
                        fuse3::Errno::new_not_exist()
                    })?
                {
                    let e621 = self.e621.clone();
                    let timeout = self.timeout;
                    let post_url = post
                        .reconstruct_missing_media_links()
                        .file
                        .url
                        .unwrap()
                        .clone();
                    let content: bytes::Bytes = Retry::spawn(
                        ExponentialBackoff::from_millis(BACKOF_DELAY)
                            .factor(BACKOF_FACTOR)
                            .max_delay(Duration::from_millis(BACKOF_MAX_DELAY))
                            .map(jitter)
                            .take(self.max_retries),
                        || e621.get_file(post_url.clone(), self.timeout),
                    )
                    .await
                    .map_err(|err| {
                        error!("reqwest::Error: {:?}", err);
                        fuse3::Errno::new_not_exist()
                    })?;

                    // reading a file typically requires multiple reads and checking this for the subsequent reads is a bit pointless
                    if self.preload_neighbors && offset == 0 {
                        let tags = custom_path.tags_to_string();
                        let max_retries = self.max_retries;
                        tokio::task::spawn(async move {
                            let posts: IndexMap<usize, e621::Post> = e621
                                .get_posts_all(&tags, max_retries, timeout)
                                .await
                                .filter_map(move |res| async {
                                    match res {
                                        Ok(post) => Some(post),
                                        Err(err) => {
                                            error!("reqwest::Error: {:?}", err);
                                            None
                                        }
                                    }
                                })
                                .map(|post| (post.id, post))
                                .collect()
                                .await;

                            let posts_sorted = posts
                                .clone()
                                .sorted_by(|id1, _, id2, _| id1.cmp(id2))
                                .collect::<IndexMap<usize, e621::Post>>();

                            [
                                posts.get_index_of(&id).map(|i| {
                                    [
                                        posts.get_index(
                                            max(usize::MIN as i64, i as i64 - 1) as usize
                                        ),
                                        posts.get_index(i + 1),
                                    ]
                                }),
                                posts_sorted.get_index_of(&id).map(|i| {
                                    [
                                        posts.get_index(
                                            max(usize::MIN as i64, i as i64 - 1) as usize
                                        ),
                                        posts.get_index(i + 1),
                                    ]
                                }),
                            ]
                            .into_iter()
                            .flatten()
                            .flat_map(|posts| {
                                posts
                                    .into_iter()
                                    .flat_map(|post| post.map(|post| post.1.clone()))
                                    .collect::<Vec<e621::Post>>()
                            })
                            .for_each(|post| {
                                debug!("preload neighbor id {:?}", post.id);

                                let e621 = e621.clone();

                                tokio::task::spawn(async move {
                                    match e621
                                        .get_file(
                                            post.reconstruct_missing_media_links()
                                                .file
                                                .url
                                                .unwrap(),
                                            timeout,
                                        )
                                        .await
                                    {
                                        Ok(_) => (),
                                        Err(err) => error!("reqwest::Error: {:?}", err),
                                    }
                                });
                            });
                        });
                    }

                    if offset as usize >= content.len() {
                        Ok(ReplyData { data: Bytes::new() })
                    } else {
                        let mut data = &content[offset as usize..];

                        if data.len() > size as usize {
                            data = &data[..size as usize];
                        }

                        Ok(ReplyData {
                            data: Bytes::copy_from_slice(data),
                        })
                    }
                } else {
                    Err(fuse3::Errno::new_not_exist())
                }
            } else {
                debug!(
                    "FS read: request: {:?}, path: {:?}, file_handle: {:?}, offset: {}, size: {}",
                    request, path, file_handle, offset, size
                );

                Err(fuse3::Errno::new_not_exist())
            }
        } else {
            Err(fuse3::Errno::new_is_dir())
        }
    }
}

#[derive(Parser, Debug, Clone)]
#[command(name = env!("CARGO_PKG_NAME"), about = "e621 as a fuse fs", version = env!("CARGO_PKG_VERSION"))]
struct Args {
    /// Mountpoint
    #[arg(
        value_parser = value_parser!(PathBuf),
    )]
    mountpoint: PathBuf,
    /// Enables verbose output, you can also use RUST_LOG env var to set the log level
    #[arg(short, long)]
    verbose: bool,
    /// Enables trace output
    #[arg(short, long)]
    trace: bool,
    /// API key can be aquired from e621.net
    #[arg(
        short,
        long,
        requires_all = &["nick"],
    )]
    api_key: Option<String>,
    /// Nick on e621.net (required for API key usage)
    #[arg(short, long)]
    nick: Option<String>,
    /// max retry attempts before giving up on a request
    #[arg(long, default_value = "3")]
    max_retries: usize,
    /// metadata cache ttl in secs
    #[arg(long, default_value = "300")]
    metadata_cache_time_secs: u64,
    /// metadata cache size in entrie count
    #[arg(long, default_value = "250")]
    metadata_cache_size: usize,
    /// file cache ttl in secs
    #[arg(long, default_value = "300")]
    file_cache_time_secs: u64,
    /// file cache size in entrie count
    #[arg(long, default_value = "200")]
    file_cache_size: usize,
    /// max page count, protects against requesting way too many pages by accident, ignored if 0 or
    /// page count larger equal 750
    #[arg(long, default_value = "0")]
    max_page_count: usize,
    /// request timeout in secs
    #[arg(long, default_value = "5.0")]
    timeout: f64,
    /// check if file is actually in the tags path, possibly requires more api requests
    #[arg(short, long)]
    check_file_path: bool,
    /// preloads the next and previous post by id and custom order
    #[arg(short, long)]
    preload_neighbors: bool,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let args = Args::parse();

    {
        let log_filter = std::env::var("RUST_LOG").unwrap_or(if args.verbose {
            String::from("debug")
        } else {
            String::from("info")
        });
        pretty_env_logger::formatted_timed_builder()
            .parse_filters(&log_filter)
            .init();
    }

    {
        tracing::subscriber::set_global_default(
            tracing_subscriber::fmt()
                .with_max_level(if args.trace {
                    tracing::Level::DEBUG
                } else {
                    tracing::Level::INFO
                })
                .finish(),
        )
        .unwrap();
    }

    debug!("{:#?}", args);

    let mut mount_options = MountOptions::default();
    mount_options
        .fs_name(env!("CARGO_PKG_NAME"))
        .force_readdir_plus(true)
        .uid(get_current_uid())
        .gid(get_current_gid())
        .read_only(true);

    Ok(Session::new(mount_options)
        .mount_with_unprivileged(
            E621Fs::new(
                args.api_key,
                args.nick,
                args.max_retries,
                args.metadata_cache_size,
                Duration::from_secs(args.metadata_cache_time_secs),
                args.file_cache_size,
                Duration::from_secs(args.file_cache_time_secs),
                args.check_file_path,
                args.preload_neighbors,
                args.timeout,
                args.max_page_count,
            ),
            args.mountpoint.as_path(),
        )
        .await?
        .await?)

    // TODO Cleanup mount point and die on umount
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_path_to_custom_path() {
        assert_eq!(
            CustomPath::try_from(PathBuf::from("/")).unwrap(),
            CustomPath {
                tags: vec![],
                has_delimiter: false,
                id: None
            }
        );

        assert_eq!(
            CustomPath::try_from(PathBuf::from("/foo/bar")).unwrap(),
            CustomPath {
                tags: vec![String::from("foo"), String::from("bar")],
                has_delimiter: false,
                id: None
            }
        );

        assert_eq!(
            CustomPath::try_from(PathBuf::from("/foo/bar/")).unwrap(),
            CustomPath {
                tags: vec![String::from("foo"), String::from("bar")],
                has_delimiter: false,
                id: None
            }
        );

        assert_eq!(
            CustomPath::try_from(PathBuf::from("/foo/bar/@")).unwrap(),
            CustomPath {
                tags: vec![String::from("foo"), String::from("bar")],
                has_delimiter: true,
                id: None
            }
        );

        assert_eq!(
            CustomPath::try_from(PathBuf::from("/foo/bar/@/")).unwrap(),
            CustomPath {
                tags: vec![String::from("foo"), String::from("bar")],
                has_delimiter: true,
                id: None
            }
        );

        assert_eq!(
            CustomPath::try_from(PathBuf::from("/@/")).unwrap(),
            CustomPath {
                tags: vec![],
                has_delimiter: true,
                id: None
            }
        );

        assert_eq!(
            CustomPath::try_from(PathBuf::from("/foo/bar/@/42")).unwrap(),
            CustomPath {
                tags: vec![String::from("foo"), String::from("bar")],
                has_delimiter: true,
                id: Some(42)
            }
        );

        assert_eq!(
            CustomPath::try_from(PathBuf::from("/foo/bar/@/42.")).unwrap(),
            CustomPath {
                tags: vec![String::from("foo"), String::from("bar")],
                has_delimiter: true,
                id: Some(42)
            }
        );

        assert_eq!(
            CustomPath::try_from(PathBuf::from("/foo/bar/@/42.png")).unwrap(),
            CustomPath {
                tags: vec![String::from("foo"), String::from("bar")],
                has_delimiter: true,
                id: Some(42)
            }
        );

        assert!(CustomPath::try_from(PathBuf::from("/foo/bar/@/fail")).is_err());

        assert!(CustomPath::try_from(PathBuf::from("/foo/bar/@/123/456.png")).is_err());
    }
}

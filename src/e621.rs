use bytes::Bytes;
use cached::{Cached, TimedSizedCache};
use chrono::{offset::FixedOffset, DateTime};
use failure::{bail, Error};
use fuse3::path::prelude::{FileAttr, FileType};
use futures_util::StreamExt;
use indexmap::map::IndexMap;
use log::{debug, warn};
use serde::{Deserialize, Deserializer};
use serde_repr::Deserialize_repr;
use serde_with::serde_as;
use std::{cmp::Ordering, mem, sync::Arc, time::Duration};
use tokio::sync::RwLock;
use tokio_retry::{
    strategy::{jitter, ExponentialBackoff},
    Retry,
};
use url::Url;
use users::{get_current_gid, get_current_uid};

use crate::{BACKOF_DELAY, BACKOF_FACTOR, BACKOF_MAX_DELAY, DEFAULT_TIME, GIT_URL};

pub const POSTS_MAX_LIMIT: usize = 320;
pub const POSTS_MAX_PAGE: usize = 750;
lazy_static::lazy_static! {
    pub static ref BASE_URL: Url = "https://e621.net/".parse().unwrap();
}

// TODO implement rate limiting
// TODO use custom future cache to stop downloading files multiple times
#[derive(Clone)]
pub struct E621 {
    api_key: Option<String>,
    nick: Option<String>,
    client: reqwest::Client,
    posts_cache: Arc<RwLock<TimedSizedCache<(String, usize, String), Posts>>>,
    file_cache: Arc<RwLock<TimedSizedCache<Url, Bytes>>>,
}

impl E621 {
    pub fn new(
        api_key: Option<String>,
        nick: Option<String>,
        posts_cache_size: usize,
        posts_cache_time: Duration,
        file_cache_size: usize,
        file_cache_time: Duration,
    ) -> Self {
        assert!(
            if api_key.is_some() {
                nick.is_some()
            } else {
                true
            },
            "Nick has to be set for api key to be used"
        );

        Self {
            client: reqwest::Client::builder()
                .user_agent(format!(
                    "{} ({})/{}{}",
                    env!("CARGO_PKG_NAME"),
                    GIT_URL,
                    env!("CARGO_PKG_VERSION"),
                    if let Some(nick) = nick.clone() {
                        format!(" by {}", nick)
                    } else {
                        String::from("")
                    }
                ))
                .build()
                .unwrap(),
            posts_cache: Arc::new(RwLock::new(TimedSizedCache::with_size_and_lifespan(
                posts_cache_size,
                posts_cache_time.as_secs(),
            ))),
            file_cache: Arc::new(RwLock::new(TimedSizedCache::with_size_and_lifespan(
                file_cache_size,
                file_cache_time.as_secs(),
            ))),
            api_key,
            nick,
        }
    }

    pub async fn get_posts(
        &self,
        tags: &str,
        limit: usize,
        page: String,
        timeout: f64,
    ) -> Result<Posts, Error> {
        if limit > POSTS_MAX_LIMIT {
            bail!("Limit is over the hard limit limit ({})", POSTS_MAX_LIMIT);
        }
        if let Ok(page) = page.parse::<usize>() {
            if page > POSTS_MAX_PAGE {
                bail!(
                    "page bigger than max page index ({}), use 'a'/'b' + id instead",
                    POSTS_MAX_PAGE
                );
            }
        }

        let cached_posts = self
            .posts_cache
            .write()
            .await
            .cache_get(&(tags.to_string(), limit, page.clone()))
            .cloned();

        match cached_posts {
            Some(posts) => Ok(posts),
            None => {
                debug!(
                    "Requesting posts: tags: \"{}\", page: {}, limit: {}",
                    tags, page, limit
                );

                let res = tokio::time::timeout(
                    Duration::from_secs_f64(timeout),
                    self.client
                        .get(build_uri(
                            BASE_URL.clone(),
                            Some(String::from("/posts.json")),
                            &self.api_key,
                            &self.nick,
                            &[
                                ("tags", tags),
                                ("limit", &limit.to_string()),
                                ("page", &page),
                            ],
                        ))
                        .send(),
                )
                .await??;

                let posts: Posts = res.json().await?;

                self.posts_cache
                    .write()
                    .await
                    .cache_set((tags.to_string(), limit, page.clone()), posts.clone());

                Ok(posts)
            }
        }
    }

    pub async fn get_posts_all<'a>(
        &'a self,
        tags: &'a str,
        retries: usize,
        timeout: f64,
    ) -> impl futures::Stream<Item = Result<Post, Error>> + 'a {
        futures::stream::unfold(1, move |i| async move {
            if i == -1 {
                None
            } else {
                let posts: Vec<Result<Post, Error>> = Retry::spawn(
                    ExponentialBackoff::from_millis(BACKOF_DELAY)
                        .factor(BACKOF_FACTOR)
                        .max_delay(Duration::from_millis(BACKOF_MAX_DELAY))
                        .map(jitter)
                        .take(retries),
                    || async {
                        self.get_posts(tags, POSTS_MAX_LIMIT, i.to_string(), timeout)
                            .await
                            .map_err(|err| {
                                warn!("error while requesting posts (possibly retrying): {}", err);
                                err
                            })
                    },
                )
                .await
                .map(|posts| posts.posts.into_iter().map(|(_, post)| Ok(post)).collect())
                .unwrap_or_else(|err| vec![Err(err)]);

                posts.iter().for_each(|post| {
                    if let Err(err) = post {
                        warn!("unfold: {:?}", err);
                    }
                });

                let posts_count = posts.len();

                match posts.first() {
                    Some(Ok(_)) => Some((
                        posts,
                        if posts_count < POSTS_MAX_LIMIT {
                            -1 // The page has less posts than requested, so there should be no more
                        } else {
                            i + 1
                        },
                    )),
                    Some(Err(_)) => Some((posts, -1)),
                    None => None,
                }
            }
        })
        .flat_map(|posts| futures::stream::iter(posts.into_iter()))
    }

    pub async fn get_posts_from_cache_only(
        &self,
        tags: String,
        limit: usize,
        page: String,
    ) -> Result<Option<Posts>, Error> {
        if limit > POSTS_MAX_LIMIT {
            bail!("Limit is over the hard limit limit ({})", POSTS_MAX_LIMIT);
        }
        if let Ok(page) = page.parse::<usize>() {
            if page > POSTS_MAX_PAGE {
                bail!(
                    "page bigger than max page index ({}), use 'a'/'b' + id instead",
                    POSTS_MAX_PAGE
                );
            }
        }

        Ok(self
            .posts_cache
            .write()
            .await
            .cache_get(&(tags, limit, page))
            .cloned())
    }

    pub async fn get_tags_all(
        &self,
        _retries: usize,
    ) -> impl futures::Stream<Item = Result<String, Error>> {
        // TODO implement with tags dump. There is also still the problem that they are a lot of tags and readdirplus needs some optiomasations first.
        // note: autocomplete JS thing of e621 website: https://github.com/zwagoth/e621ng/blob/master/app/javascript/src/javascripts/autocomplete.js.erb

        //let tags = include_str!("comma_seperated_tags_file_path").trim_end().split(",").into_iter().map(|tag| String::from(tag)).collect();
        //Ok(futures::stream::iter(self.tags.iter().cloned()))

        futures::stream::iter([].into_iter())
    }

    pub async fn get_file(&self, uri: Url, timeout: f64) -> Result<Bytes, Error> {
        let cached_file = self.file_cache.write().await.cache_get(&uri).cloned();

        match cached_file {
            Some(file) => Ok(file),
            None => {
                debug!("Requesting file: uri: \"{}\"", uri);

                let file = tokio::time::timeout(
                    Duration::from_secs_f64(timeout),
                    self.client.get(uri.clone()).send(),
                )
                .await??
                .bytes()
                .await?;

                self.file_cache.write().await.cache_set(uri, file.clone());

                Ok(file)
            }
        }
    }

    pub async fn get_file_from_cache_only(&self, uri: Url) -> Result<Option<Bytes>, Error> {
        Ok(self.file_cache.write().await.cache_get(&uri).cloned())
    }

    pub async fn is_post_in_search(
        &self,
        id: usize,
        tags: Option<&str>,
        get_uncached_posts: bool,
        timeout: f64,
    ) -> Result<Option<Post>, Error> {
        let mut posts_cache = self.posts_cache.write().await;

        let cache_keys: Vec<(String, usize, String)> = posts_cache.key_order().cloned().collect();
        for (tags_cache, limit_cache, page_cache) in cache_keys {
            if tags.is_none() || tags.unwrap() == tags_cache {
                if let Some(posts) = posts_cache.cache_get(&(tags_cache, limit_cache, page_cache)) {
                    for (id_cache, post_cache) in posts.clone().posts {
                        if id == id_cache {
                            return Ok(Some(post_cache));
                        }
                    }
                }
            }
        }

        mem::drop(posts_cache);

        if get_uncached_posts {
            let tags_request = format!(
                "id:{}{}",
                id,
                match tags {
                    Some(tags) => format!(" {}", tags),
                    None => String::from(""),
                }
            );

            debug!(
                "Requesting posts: tags: \"{}\", page: {}, limit: {}",
                tags_request, 1, 1
            );

            return Ok(self
                .get_posts(&tags_request, 1, 1.to_string(), timeout)
                .await?
                .posts
                .into_iter()
                .next()
                .map(|(_id, post)| post));
        }

        Ok(None)
    }
}

pub fn build_uri(
    mut url: Url,
    path: Option<String>,
    api_key: &Option<String>,
    nick: &Option<String>,
    query: &[(&str, &str)],
) -> Url {
    if let Some(path) = path {
        url.set_path(&path);
    }

    if !query.is_empty() || (api_key.is_some() && nick.is_some()) {
        let mut url_query = url.query_pairs_mut();

        if let (Some(api_key), Some(nick)) = (api_key, nick) {
            url_query
                .append_pair("api_key", api_key)
                .append_pair("login", nick);
        }

        for (key, value) in query.iter() {
            url_query.append_pair(key, value);
        }
    }

    url.to_string().parse().unwrap()
}

#[derive(Deserialize, Debug, Clone)]
pub struct Posts {
    #[serde(deserialize_with = "deserialize_posts")]
    pub posts: IndexMap<usize, Post>,
}

// For finding all nullable fields: https://github.com/zwagoth/e621ng/blob/master/db/structure.sql#L1888
#[derive(Deserialize, Debug, Clone)]
pub struct Post {
    pub id: usize,
    pub created_at: Option<DateTime<FixedOffset>>,
    pub updated_at: Option<DateTime<FixedOffset>>,
    pub file: File,
    pub preview: Preview,
    pub sample: Sample,
    pub score: Score,
    pub tags: PostTags,
    pub locked_tags: Option<Vec<String>>,
    pub change_seq: usize,
    pub flags: Flags,
    pub rating: Rating,
    pub fav_count: usize,
    //#[serde_as(as = "Vec<serde_with::DisplayFromStr>")]
    //sources: Vec<Url>,
    pub source: Option<Vec<String>>, // It's a user definable field, so sadly there are not only valid urls in there...
    pub pools: Vec<usize>,
    pub relationships: Relationships,
    pub approver_id: Option<usize>,
    pub uploader_id: usize,
    pub description: String,
    pub comment_count: usize,
    pub is_favorited: bool,
    pub has_notes: bool,
    pub duration: Option<f64>,
}

impl PartialOrd for Post {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Post {
    fn cmp(&self, other: &Self) -> Ordering {
        self.id.cmp(&other.id)
    }
}

impl PartialEq for Post {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Eq for Post {}

#[serde_as]
#[derive(Deserialize, Debug, Clone)]
pub struct File {
    pub width: isize,
    pub height: isize,
    pub ext: String,
    pub size: usize,
    pub md5: String,
    #[serde_as(as = "Option<serde_with::DisplayFromStr>")]
    pub url: Option<Url>, // is null if blacklisted by e621
}

#[serde_as]
#[derive(Deserialize, Debug, Clone)]
pub struct Sample {
    pub width: usize,
    pub height: usize,
    #[serde_as(as = "Option<serde_with::DisplayFromStr>")]
    pub url: Option<Url>, // is null if blacklisted by e621
}

#[serde_as]
#[derive(Deserialize, Debug, Clone)]
pub struct Preview {
    pub width: usize,
    pub height: usize,
    #[serde_as(as = "Option<serde_with::DisplayFromStr>")]
    pub url: Option<Url>, // is null if blacklisted by e621
}

#[derive(Deserialize, Debug, Clone)]
pub struct Score {
    pub up: usize,
    pub down: isize, // 1 Downvote is a by 1 decrease in this number
    pub total: isize,
}

#[derive(Deserialize, Debug, Clone)]
pub struct PostTags {
    pub general: Vec<String>,
    pub species: Vec<String>,
    pub character: Vec<String>,
    pub copyright: Vec<String>,
    pub artist: Vec<String>,
    pub invalid: Vec<String>,
    pub lore: Vec<String>,
    pub meta: Vec<String>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Flags {
    pub pending: bool,
    pub flagged: bool,
    pub note_locked: bool,
    pub status_locked: bool,
    pub rating_locked: bool,
    pub deleted: bool,
}

#[derive(Deserialize, Debug, Clone)]
pub enum Rating {
    #[serde(rename = "s")]
    Safe,
    #[serde(rename = "q")]
    Questionable,
    #[serde(rename = "e")]
    Explicit,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Relationships {
    pub parent_id: Option<usize>,
    pub has_children: bool,
    pub has_active_children: bool,
    pub children: Vec<usize>,
}

fn deserialize_posts<'de, D>(deserializer: D) -> Result<IndexMap<usize, Post>, D::Error>
where
    D: Deserializer<'de>,
{
    Ok(Vec::<Post>::deserialize(deserializer)?
        .into_iter()
        .map(|post| (post.id, post))
        .collect())
}

impl Posts {
    // This function tries to guess the media links for backlisted images, no checking is done
    pub fn reconstruct_missing_media_links(self) -> Self {
        Posts {
            posts: self
                .posts
                .into_iter()
                .map(|(id, post)| (id, post.reconstruct_missing_media_links()))
                .collect(),
        }
    }
}

impl Post {
    pub fn reconstruct_missing_media_links(mut self) -> Self {
        if self.file.url.is_none() {
            self.file.url = Some(
                format!(
                    "https://static1.e621.net/data/{}/{}/{}.{}",
                    self.file.md5.get(0..2).unwrap(),
                    self.file.md5.get(2..4).unwrap(),
                    self.file.md5,
                    self.file.ext
                )
                .parse()
                .unwrap(),
            );
        }
        if self.sample.url.is_none() {
            self.sample.url = Some(
                format!(
                    "https://static1.e621.net/data/sample/{}/{}/{}.jpg",
                    self.file.md5.get(0..2).unwrap(),
                    self.file.md5.get(2..4).unwrap(),
                    self.file.md5,
                )
                .parse()
                .unwrap(),
            );
        }
        if self.preview.url.is_none() {
            self.preview.url = Some(
                format!(
                    "https://static1.e621.net/data/preview/{}/{}/{}.jpg",
                    self.file.md5.get(0..2).unwrap(),
                    self.file.md5.get(2..4).unwrap(),
                    self.file.md5,
                )
                .parse()
                .unwrap(),
            );
        }

        self
    }
}

#[allow(clippy::from_over_into)] // Not possible to implement From in this case
impl Into<FileAttr> for &Post {
    fn into(self) -> FileAttr {
        FileAttr {
            size: self.file.size as u64,
            blocks: self.file.size as u64,
            atime: self
                .updated_at
                .map(|time| time.into())
                .unwrap_or(DEFAULT_TIME),
            mtime: self
                .updated_at
                .map(|time| time.into())
                .unwrap_or(DEFAULT_TIME),
            ctime: self
                .created_at
                .map(|time| time.into())
                .unwrap_or(DEFAULT_TIME),
            kind: FileType::RegularFile,
            perm: fuse3::perm_from_mode_and_kind(FileType::RegularFile, 0o444),
            nlink: 0,
            uid: get_current_uid(),
            gid: get_current_gid(),
            rdev: 0,
            blksize: 1,
        }
    }
}

#[derive(Deserialize, Debug, Clone)]
#[serde(transparent)]
pub struct Tags {
    #[serde(deserialize_with = "deserialize_tags")]
    pub tags: IndexMap<usize, Tag>,
}

// For finding all nullable fields: https://github.com/zwagoth/e621ng/blob/master/db/structure.sql#L2245
#[derive(Deserialize, Debug, Clone)]
pub struct Tag {
    pub id: usize,
    pub name: String,
    pub post_count: usize,
    pub related_tags: Option<String>, // Space seperated list, but unshure what the number next to every tag is
    pub related_tags_updated_at: Option<DateTime<FixedOffset>>,
    pub category: TagCategory,
    pub is_locked: bool,
    pub created_at: DateTime<FixedOffset>,
    pub updated_at: DateTime<FixedOffset>,
}

#[derive(Deserialize_repr, Debug, Clone)]
#[repr(usize)]
pub enum TagCategory {
    General = 0,
    Artist = 1,
    Copyright = 3,
    Character = 4,
    Species = 5,
    Invalid = 6,
    Meta = 7,
    Lore = 8,
}

fn deserialize_tags<'de, D>(deserializer: D) -> Result<IndexMap<usize, Tag>, D::Error>
where
    D: Deserializer<'de>,
{
    Ok(Vec::<Tag>::deserialize(deserializer)?
        .into_iter()
        .map(|tag| (tag.id, tag))
        .collect())
}

#[allow(clippy::from_over_into)] // Not possible to implement From in this case
impl Into<Vec<String>> for &Tags {
    fn into(self) -> Vec<String> {
        self.tags
            .iter()
            .map(|(_id, tag)| tag.name.clone())
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    impl E621 {
        fn new_testing() -> E621 {
            Self::new(
                None,
                None,
                1,
                Duration::from_secs(60),
                1,
                Duration::from_secs(60),
            )
        }
    }

    #[test]
    fn build_uri() {
        assert_eq!(
            super::build_uri(
                "https://example.com/".parse::<Url>().unwrap(),
                None,
                &None,
                &None,
                &[]
            ),
            "https://example.com/".parse::<Url>().unwrap()
        );

        assert_eq!(
            super::build_uri(
                "https://example.com/".parse::<Url>().unwrap(),
                None,
                &None,
                &None,
                &[("foo", "bar")]
            ),
            "https://example.com/?foo=bar".parse::<Url>().unwrap()
        );

        assert_eq!(
            super::build_uri(
                "https://example.com/".parse::<Url>().unwrap(),
                Some(String::from("/test/path")),
                &None,
                &None,
                &[]
            ),
            "https://example.com/test/path".parse::<Url>().unwrap()
        );

        assert_eq!(
            super::build_uri(
                "https://example.com/".parse::<Url>().unwrap(),
                None,
                &Some(String::from("api_key")),
                &Some(String::from("nick")),
                &[]
            ),
            "https://example.com/?api_key=api_key&login=nick"
                .parse::<Url>()
                .unwrap()
        );

        assert_eq!(
            super::build_uri(
                "https://example.com/".parse::<Url>().unwrap(),
                None,
                &Some(String::from("api_key")),
                &None,
                &[]
            ),
            "https://example.com/".parse::<Url>().unwrap()
        );

        assert_eq!(
            super::build_uri(
                "https://example.com/".parse::<Url>().unwrap(),
                None,
                &None,
                &Some(String::from("nick")),
                &[]
            ),
            "https://example.com/".parse::<Url>().unwrap()
        );

        assert_eq!(
            super::build_uri(
                "https://example.com/".parse::<Url>().unwrap(),
                Some(String::from("test/path")),
                &Some(String::from("api_key")),
                &Some(String::from("nick")),
                &[("foo", "bar")]
            ),
            "https://example.com/test/path?api_key=api_key&login=nick&foo=bar"
                .parse::<Url>()
                .unwrap()
        );
    }

    #[test]
    fn post_into_file_attr() {
        assert_eq!(
            {
                let post: FileAttr = (&Post {
                    id: 0,
                    created_at: None,
                    updated_at: None,
                    file: File {
                        width: 0,
                        height: 0,
                        ext: String::from("png"),
                        size: 0,
                        md5: String::from("d41d8cd98f00b204e9800998ecf8427e"),
                        url: None,
                    },
                    preview: Preview {
                        width: 0,
                        height: 0,
                        url: None,
                    },
                    sample: Sample {
                        width: 0,
                        height: 0,
                        url: None,
                    },
                    score: Score {
                        up: 0,
                        down: 0,
                        total: 0,
                    },
                    tags: PostTags {
                        general: vec![],
                        species: vec![],
                        character: vec![],
                        copyright: vec![],
                        artist: vec![],
                        invalid: vec![],
                        lore: vec![],
                        meta: vec![],
                    },
                    locked_tags: None,
                    change_seq: 0,
                    flags: Flags {
                        pending: false,
                        flagged: false,
                        note_locked: false,
                        status_locked: false,
                        rating_locked: false,
                        deleted: false,
                    },
                    rating: Rating::Safe,
                    fav_count: 0,
                    source: None,
                    pools: vec![],
                    relationships: Relationships {
                        parent_id: None,
                        has_children: false,
                        has_active_children: false,
                        children: vec![],
                    },
                    approver_id: None,
                    uploader_id: 0,
                    description: String::from(""),
                    comment_count: 0,
                    is_favorited: false,
                    has_notes: false,
                    duration: None,
                })
                    .into();

                post
            },
            FileAttr {
                size: 0,
                blocks: 0,
                atime: DEFAULT_TIME,
                mtime: DEFAULT_TIME,
                ctime: DEFAULT_TIME,
                kind: FileType::RegularFile,
                perm: fuse3::perm_from_mode_and_kind(FileType::RegularFile, 0o444),
                nlink: 0,
                uid: get_current_uid(),
                gid: get_current_gid(),
                rdev: 0,
                blksize: 1,
            }
        );

        assert_eq!(
            {
                let post: FileAttr = (&Post { // Requested on 2021-08-06 00:00
                        id: 1411608,
                        created_at: Some(
                            DateTime::parse_from_rfc3339("2017-12-28T12:34:28.935-05:00").unwrap(),
                        ),
                        updated_at: Some(
                            DateTime::parse_from_rfc3339("2021-08-05T12:16:30.069-04:00").unwrap(),
                        ),
                        file: File {
                            width: 2499,
                            height: 3042,
                            ext: String::from("png"),
                            size: 835206,
                            md5: String::from("4d337eff988fc1a4ec90b7c9d5de09f6"),
                            url: Some(String::from("https://static1.e621.net/data/4d/33/4d337eff988fc1a4ec90b7c9d5de09f6.png").parse::<Url>().unwrap()),
                        },
                        preview: Preview {
                            width: 123,
                            height: 150,
                            url: Some(String::from("https://static1.e621.net/data/preview/4d/33/4d337eff988fc1a4ec90b7c9d5de09f6.png").parse::<Url>().unwrap()),
                        },
                        sample: Sample {
                            width: 850,
                            height: 1034,
                            url: Some(String::from("https://static1.e621.net/data/sample/4d/33/4d337eff988fc1a4ec90b7c9d5de09f6.png").parse::<Url>().unwrap()),
                        },
                        score: Score {
                            up: 332,
                            down: -3,
                            total: 329,
                        },
                        tags: PostTags {
                            general: vec![
                                String::from("3_toes"),
                                String::from("5_fingers"),
                                String::from("ambiguous_gender"),
                                String::from("anthro"),
                                String::from("blue_background"),
                                String::from("blue_body"),
                                String::from("chibi"),
                                String::from("crouching"),
                                String::from("dipstick_ears"),
                                String::from("feet"),
                                String::from("fingers"),
                                String::from("fluffy"),
                                String::from("fluffy_tail"),
                                String::from("looking_at_viewer"),
                                String::from("machine"),
                                String::from("mascot"),
                                String::from("multicolored_ears"),
                                String::from("nibbling"),
                                String::from("outline"),
                                String::from("simple_background"),
                                String::from("solo"),
                                String::from("toes"),
                                String::from("yellow_eyes"),
                                String::from("yellow_nose"),
                                String::from("young"),
                            ],
                            species: vec![
                                String::from("android"),
                                String::from("domestic_cat"),
                                String::from("felid"),
                                String::from("feline"),
                                String::from("felis"),
                                String::from("mammal"),
                                String::from("robot"),
                            ],
                            character: vec![
                                String::from("esix"),
                            ],
                            copyright: vec![
                                String::from("e621"),
                            ],
                            artist: vec![
                                String::from("tricksta"),
                            ],
                            invalid: vec![],
                            lore: vec![],
                            meta: vec![
                                String::from("2017"),
                                String::from("absurd_res"),
                                String::from("digital_drawing_(artwork)"),
                                String::from("digital_media_(artwork)"),
                                String::from("hi_res"),
                            ],
                        },
                        locked_tags: Some(
                            vec![],
                        ),
                        change_seq: 34210483,
                        flags: Flags {
                            pending: false,
                            flagged: false,
                            note_locked: false,
                            status_locked: false,
                            rating_locked: false,
                            deleted: false,
                        },
                        rating: Rating::Safe,
                        fav_count: 620,
                        source: None,
                        pools: vec![],
                        relationships: Relationships {
                            parent_id: None,
                            has_children: true,
                            has_active_children: true,
                            children: vec![
                                1414338,
                                1414339,
                                1514912,
                                2068979,
                            ],
                        },
                        approver_id: Some(
                            169756,
                        ),
                        uploader_id: 291235,
                        description: String::from("Random Esix for you guys. My first upload here go easy on me and my lackluster tagging ability."),
                        comment_count: 17,
                        is_favorited: false,
                        has_notes: false,
                        duration: None,
                    }).into();

                post
            },
            FileAttr {
                size: 835206,
                blocks: 835206,
                atime: DateTime::parse_from_rfc3339("2021-08-05T12:16:30.069-04:00")
                    .unwrap()
                    .into(),
                mtime: DateTime::parse_from_rfc3339("2021-08-05T12:16:30.069-04:00")
                    .unwrap()
                    .into(),
                ctime: DateTime::parse_from_rfc3339("2017-12-28T12:34:28.935-05:00")
                    .unwrap()
                    .into(),
                kind: FileType::RegularFile,
                perm: fuse3::perm_from_mode_and_kind(FileType::RegularFile, 0o444),
                nlink: 0,
                uid: get_current_uid(),
                gid: get_current_gid(),
                rdev: 0,
                blksize: 1,
            }
        );
    }

    #[tokio::test]
    #[ignore = "requires internet"]
    async fn request_posts() {
        let e621 = E621::new_testing();

        assert!(!e621
            .get_posts("id:1411608", 1, 1.to_string(), 10.0)
            .await
            .unwrap()
            .posts
            .is_empty(),
            "Didn't find post 1411608 (it's possible that it doesn't exist anymore, but that is unlikely)");

        assert!(!e621
            .get_posts_from_cache_only(String::from("id:1411608"), 1, 1.to_string())
            .await
            .unwrap()
            .expect("Expected to find post in cache")
            .posts
            .is_empty());

        assert!(
            e621.is_post_in_search(1411608, None, false, 10.0)
                .await
                .unwrap()
                .is_some(),
            "Expected to find post in cache"
        );

        assert!(
            e621.is_post_in_search(1411608, Some("id:1411608"), false, 10.0)
                .await
                .unwrap()
                .is_some(),
            "Expected to find post in cache"
        );
    }

    #[tokio::test]
    #[ignore = "requires internet"]
    async fn request_file() {
        let e621 = E621::new_testing();

        let uri: Url =
            "https://static1.e621.net/data/preview/4d/33/4d337eff988fc1a4ec90b7c9d5de09f6.png"
                .parse()
                .unwrap();

        assert!(!e621
            .get_file(uri.clone(), 10.0)
            .await
            .expect("Didn't find preview of post 1411608 (it's possible that it doesn't exist anymore, but that is unlikely)")
            .is_empty());

        assert!(!e621
            .get_file_from_cache_only(uri)
            .await
            .unwrap()
            .expect("Expected to find file in cache")
            .is_empty());
    }

    #[test]
    fn reconstruct_missing_media_links() {
        let posts = Posts {
            posts: {
                let mut posts = IndexMap::new();

                posts.insert(
                    0,
                    Post {
                        id: 0,
                        created_at: None,
                        updated_at: None,
                        file: File {
                            width: 0,
                            height: 0,
                            ext: String::from("png"),
                            size: 0,
                            md5: String::from("d41d8cd98f00b204e9800998ecf8427e"),
                            url: None,
                        },
                        preview: Preview {
                            width: 0,
                            height: 0,
                            url: None,
                        },
                        sample: Sample {
                            width: 0,
                            height: 0,
                            url: None,
                        },
                        score: Score {
                            up: 0,
                            down: 0,
                            total: 0,
                        },
                        tags: PostTags {
                            general: vec![],
                            species: vec![],
                            character: vec![],
                            copyright: vec![],
                            artist: vec![],
                            invalid: vec![],
                            lore: vec![],
                            meta: vec![],
                        },
                        locked_tags: None,
                        change_seq: 0,
                        flags: Flags {
                            pending: false,
                            flagged: false,
                            note_locked: false,
                            status_locked: false,
                            rating_locked: false,
                            deleted: false,
                        },
                        rating: Rating::Safe,
                        fav_count: 0,
                        source: None,
                        pools: vec![],
                        relationships: Relationships {
                            parent_id: None,
                            has_children: false,
                            has_active_children: false,
                            children: vec![],
                        },
                        approver_id: None,
                        uploader_id: 0,
                        description: String::from(""),
                        comment_count: 0,
                        is_favorited: false,
                        has_notes: false,
                        duration: None,
                    },
                );

                posts
            },
        };

        let (_, post) = posts
            .reconstruct_missing_media_links()
            .posts
            .into_iter()
            .next()
            .unwrap();

        assert_eq!(
            post.file.url,
            Some(
                "https://static1.e621.net/data/d4/1d/d41d8cd98f00b204e9800998ecf8427e.png"
                    .parse::<Url>()
                    .unwrap()
            )
        );
        assert_eq!(
            post.sample.url,
            Some(
                "https://static1.e621.net/data/sample/d4/1d/d41d8cd98f00b204e9800998ecf8427e.jpg"
                    .parse::<Url>()
                    .unwrap()
            )
        );
        assert_eq!(
            post.preview.url,
            Some(
                "https://static1.e621.net/data/preview/d4/1d/d41d8cd98f00b204e9800998ecf8427e.jpg"
                    .parse::<Url>()
                    .unwrap()
            )
        );
    }

    #[tokio::test]
    async fn get_tags() {
        let e621 = E621::new_testing();

        let tags: Vec<String> = e621
            .get_tags_all(5)
            .await
            .map(|tag| tag.unwrap())
            .collect()
            .await;

        println!("tags_count: {}", tags.len());
    }
}
